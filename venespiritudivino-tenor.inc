\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key d \major

		R1*2  |
		r4 d 8 d d d e fis  |
		e 8 b, ~ b, 2.  |
%% 5
		r8 cis cis cis cis 4 b, 8 a,  |
		g 4. ( fis 8 ) fis 2  |
		r8 a a a a 8. b a 8  |
		a 8 g ~ g 2.  |
		r8 g g g g 8. d e 8  |
%% 10
		fis 4 g 8 fis ~ fis 2  |
		r8 d d d d 8. e fis 8  |
		e 8 b, ~ b, 2.  |
		r8 e e e e 8. fis g 8  |
		\time 2/4
		g 4. ( fis 8 )  |
%% 15
		\time 4/4
		fis 2 r  |
		r8 d d d d 8. e fis 8  |
		e 8 b, ~ b, 2.  |
		r8 cis cis cis cis 4 b, 8 a,  |
		g 4. ( fis 8 ) fis 2  |
%% 20
		r8 a a a a 8. b a 8  |
		a 8 g ~ g 2.  |
		r8 g g g g 8. d e 8  |
		fis 4 ( g 8 ) fis ~ fis 2  |
		r8 d d d d 8. e fis 8  |
%% 25
		e 8 b, 16 b, ~ b, 2.  |
		r8 e e e e 8. fis g 8  |
		\time 2/4
		g 4. ( fis 8 )  |
		\time 4/4
		fis 2 r  |
		r8 d d d d 8. e fis 8  |
%% 30
		e 8 b, ~ b, 2.  |
		r8 cis cis cis cis 4 b, 8 a,  |
		g 4. fis 8 fis 2  |
		r8 a a a a 8. b a 8  |
		a 8 g ~ g 2.  |
%% 35
		r8 g g g g 8. d e 8  |
		fis 4 ( g 8 ) fis ~ fis 2  |
		r8 d d d d 8. e fis 8  |
		e 8 b, ~ b, 2.  |
		r8 e e e e 8. fis g 8  |
%% 40
		\time 2/4
		g 4. ( fis 8 )  |
		\time 4/4
		fis 2 r  |
		r8 d d d d 8. e fis 8  |
		e 8 b, ~ b, 2.  |
		r8 cis cis cis cis 4 b, 8 a,  |
%% 45
		g 4. ( fis 8 ) fis 2  |
		r8 a a a a 8. b a 8  |
		a 8 g ~ g 2.  |
		r8 g g g g 8. d e 8  |
		fis 4 ( g 8 ) fis ~ fis 2  |
%% 50
		r8 d d d d 8. e fis 8  |
		e 8 b, 16 b, ~ b, 2.  |
		r8 e e e e 8. fis g 8  |
		\time 2/4
		g 4. ( fis 8 )  |
		\time 4/4
		fis 2 r  |
%% 55
		r8 d d d d 8. e fis 8  |
		e 8 b, ~ b, 2.  |
		r8 cis cis cis cis 4 b, 8 a,  |
		g 4. ( fis 8 ) fis 2  |
		r8 a a a a 8. b a 8  |
%% 60
		a 8 g ~ g 2.  |
		r8 g g g g 8. d e 8  |
		fis 4 g 8 fis ~ fis 2  |
		r8 d d d d 8. e fis 8  |
		e 8 b, ~ b, 2.  |
%% 65
		r8 e e e e 8. fis g 8  |
		\time 2/4
		g 4. ( fis 8 )  |
		\time 4/4
		fis 2 r4 a,  |
		e 2. g 4  |
		fis 1  |
%% 70
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Ven, Es -- pí -- ri -- tu di -- vi -- no, __
		man -- da tu luz des -- "de el" cie -- lo.
		pa -- "dre a" -- mo -- ro -- so del po -- bre, __
		don en tus do -- nes es -- plén -- di -- do, __
		luz que pe -- ne -- tra las al -- mas, __
		fuen -- te del ma -- yor con -- sue -- lo.

		Ven, dul -- ce hués -- ped del al -- ma, __
		des -- can -- so de nues -- "tro es" -- fuer -- zo;
		tre -- "gua en" el du -- ro tra -- ba -- jo, __
		bri -- "sa en" las ho -- ras de fue -- go, __
		go -- zo "que en" -- ju -- ga las lá -- gri -- mas __
		y re -- con -- for -- "ta en" los due -- los.

		En -- "tra has" -- "ta el" fon -- do del al -- ma, __
		di -- vi -- na luz "y en" -- ri -- qué -- ce -- nos.
		Mi -- "ra el" va -- cí -- o del hom -- bre __
		si tú le fal -- tas por den -- tro; __
		mi -- "ra el" po -- der del pe -- ca -- do __
		cuan -- do "no en" -- ví -- as "tu a" -- lien -- to.

		Rie -- ga la tie -- "rra en" se -- quí -- a, __
		sa -- "na el" co -- ra -- zón en -- fer -- mo,
		la -- va las man -- chas, in -- fun -- de __
		ca -- lor de vi -- "da en" el hie -- lo; __
		do -- "ma el" es -- pí -- ri -- "tu in" -- dó -- mi -- to, __
		guí -- "a al" que tuer -- "ce el" sen -- de -- ro.

		Re -- par -- te tus sie -- te do -- nes __
		se -- gún la fe de tus sier -- vos;
		por tu bon -- dad y tu gra -- cia __
		da -- "le al" es -- fuer -- zo su mé -- ri -- to; __
		sal -- "va al" que bus -- ca sal -- var -- se, __
		y da -- nos tu go -- "zo e" -- ter -- no.

		A -- mén, a -- mén.
	}
>>
