\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% intro
		d1 d1

		% ven, espiritu divino...
		d1 e1:m a1 g2 d2
		a2:m d2:7 g1 g1:m d1
		b1:m e1:m a1
		g2 d1

		% ven, dulce huesped del alma...
		d1 e1:m a1 g2 d2
		a2:m d2:7 g1 g1:m d1
		b1:m e1:m a1
		g2 d1

		%
		d1 e1:m a1 g2 d2
		a2:m d2:7 g1 g1:m d1
		b1:m e1:m a1
		g2 d1

		%
		d1 e1:m a1 g2 d2
		a2:m d2:7 g1 g1:m d1
		b1:m e1:m a1
		g2 d1

		%
		d1 e1:m a1 g2 d2
		a2:m d2:7 g1 g1:m d1
		b1:m e1:m a1
		g2 d1

		% amen, amen
		g1 d1
	}
